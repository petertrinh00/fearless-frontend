import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeForm from './AttendeeForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { BrowserRouter, Route, Routes, NavLink } from "react-router-dom";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />

      {/* <AttendeeForm /> */}
      {/* <ConferenceForm /> */}
      {/* <PresentationForm /> */}
      <Routes>
        <Route index element={<MainPage />} />

        <Route path="presentations">
          <Route path="new" element={<PresentationForm />} />
        </Route>
        <Route path="attendees">
          <Route path="new" element={<AttendeeForm />} />
        </Route>
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />} />
        </Route>
        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
        </Route>
        <Route path='attendees'>
          <Route index element={<AttendeesList attendees={props.attendees} />} />
        </Route>
      </Routes>
      {/* <AttendeesList attendees={props.attendees} /> */}

    </BrowserRouter>
  );
}

export default App;
